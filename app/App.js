import React, {Fragment, useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/styles/index.scss';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Shell from './components/Shell';
import Store from "./store/store";

const App = () => {

    return (
        <Fragment>
            <Store>
                <Router>
                    <Route path="/" component={Shell} />
                </Router>
            </Store>
        </Fragment>

    )
};

export default App;