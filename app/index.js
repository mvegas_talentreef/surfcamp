import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App";
import '../app/assets/styles/index.scss';

// rsc, rscp
ReactDOM.render(<App />, document.getElementById('root'));