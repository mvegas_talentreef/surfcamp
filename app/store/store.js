import React, { useState } from 'react';
import AppSettings from '../config/appSettings';
import BrandSettings from '../config/brandSettings';

export const SettingsContext = React.createContext(false);
export const AppContext = React.createContext({});
export const BrandContext = React.createContext({});
export const UserContext = React.createContext({});
export const LoadingContext = React.createContext(false);
export const NotificationContext = React.createContext(false);

export const SettingsConsumer = SettingsContext.Consumer;
export const AppConsumer = AppContext.Consumer;
export const BrandConsumer = BrandContext.Consumer;
export const UserConsumer = UserContext.Consumer;
export const LoadingConsumer = LoadingContext.Consumer;
export const NotificationConsumer = NotificationContext.Consumer;

const Store = ({ children }) => {
    const [appSettings, setAppSettings] = useState(AppSettings.data[0]);
    const [brandSettings, setBrandSettings] = useState(BrandSettings.data[0]);
    const [user, setUser] = useState({});
    const [loading, setLoading] = useState(false);
    const [notification, setNotification] = useState('');
    const [showConfig, setShowConfig] = useState(false);

    return (
        <SettingsContext.Provider value={[showConfig, setShowConfig]}>
            <AppContext.Provider value={[appSettings, setAppSettings]}>
                <BrandContext.Provider value={[brandSettings, setBrandSettings]}>
                    <LoadingContext.Provider value={[loading, setLoading]}>
                        <NotificationContext.Provider value={[notification, setNotification]}>
                            <UserContext.Provider value={[user, setUser]}>
                                {children}
                            </UserContext.Provider>
                        </NotificationContext.Provider>
                    </LoadingContext.Provider>
                </BrandContext.Provider>
            </AppContext.Provider>
        </SettingsContext.Provider>
    )
};

export default Store;
