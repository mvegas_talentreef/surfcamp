import React, { createContext, useContext, useReducer } from 'react';
import rootStore from "./store";

// state context contains both Provider and Consumer
export const StateContext = createContext({});

// export const UserConsumer = StoreContext.Consumer;
// export const UserProvider = StoreContext.Provider;

// new React component called StateProvider
export const StateProvider = ({reducer, initialState, children }) => (
    <StateContext.Provider value={useReducer(reducer, initialState )}>
        {children}
    </StateContext.Provider>
);

export const StateConsumer = (children) => (
        <StateContext.Consumer>
            {children}
        </StateContext.Consumer>
);


export const useStateValue = () => useContext(StateContext);