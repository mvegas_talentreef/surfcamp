import React from 'react';
import PageHeader from "../_base/PageHeader/PageHeader";

const Resources = () => {
    return (
        <PageHeader title="Resources" utilityComponent={null} />
    )
};

export default Resources;