import React from 'react';
import PageHeader from "../_base/PageHeader/PageHeader";

const Help = () => {
    return (
        <PageHeader title="Help" utilityComponent={null} />
    )
};

export default Help;