import React, {Fragment, useContext, useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import './../assets/styles/index.scss';
import LocationsContainer from "../containers/locations/LocationsContainer";
import {BrowserRouter as Router, Route, Switch } from "react-router-dom";
import DashboardContainer from "../containers/dashboard/DashboardContainer";
import IntegrationsContainer from "./../containers/IntegrationsContainer";
import ApplicantsContainer from "../containers/applicants/ApplicantsContainer";
import UsersContainer from "../containers/users/UsersContainer";
import ReportsContainer from "../containers/reports/ReportsContainer";
import OrganizationContainer from "../containers/organization/OrganizationContainer";
import OrgSettingsContainer from "../containers/organization/OrgSettingsContainer";
import HelpContainer from "../containers/help/HelpContainer";
import EmployeesContainer from "../containers/employees/EmployeesContainer";
import ProfileContainer from "../containers/account/ProfileContainer";
import ResourcesContainer from "../containers/resources/ResourcesContainer";
import SettingsContainer from "../containers/account/SettingsContainer";
import Posting from "./postings/Posting";
import Button from '@material-ui/core/Button';
import { FormControlLabel, Checkbox } from '@material-ui/core';

// configuration dialog
import Dialog from '@material-ui/core/Dialog';
import SignInContainer from "../containers/account/SignInContainer";
import PostDetail from "./postings/PostDetail";
import NavBars from "./_shell/NavBars";
import Loading from "./_base/Loading/Loading";
import {SettingsContext, AppContext, BrandContext, LoadingContext, NotificationContext} from "../store/store";
import SnackbarWrapper from "./_base/Snackbar/SnackbarWrapper";
import BrandSettings from '../config/brandSettings';
import Roles from '../mockData/roles';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'block',
        padding: theme.spacing(4, 3),
        marginBottom: 12,
        color: '#777777',
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        paddingRight: 40,
        marginLeft: 60
    },
    portalDialog: {
      margin: '24px'
    },
    portal: {
        padding: '40px'
    }
}));

const Shell = () => {

    const classes = useStyles();
    const [showConfig, setShowConfig] = useContext(SettingsContext);
    const [loading, setLoading] = useContext(LoadingContext);
    const [notification, setNotification] = useContext(NotificationContext);
    const [brandSettings, setBrandSettings] = useContext(BrandContext);

    // app settings
    const [applicantTracking, setApplicantTracking] = useState(false);
    const [recruit, setRecruit] = useState(false);
    const [employeeManagement, setEmployeeManagement] = useState(false);

    const toggleConfig = () => {
        setShowConfig(!showConfig);
    };

    const handleCloseSnack = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setNotification('');
    };

    const saveConfig = (e) => {
        setLoading(true);
        setTimeout(() => {
            let selectedClient = document.getElementById('client').value;
            setBrandSettings(BrandSettings.data[parseInt(selectedClient)]);
            setLoading(false);
            setNotification('Platform Configurations Saved!');
        }, 2000);
    };

    const handleChange = name => event => {
        console.log(name);
        event.target.checked;
        // event.target.checked;
        switch(name) {
            case "applicanttracking":
                setApplicantTracking(!applicantTracking);
                break;
            case "recruit":
                setRecruit(!recruit);
                break;
            case "employeemanagement":
                setEmployeeManagement(!employeeManagement);
                break;
        }
    };

    return (
        <Router>
            <div className={classes.root}>
                <NavBars />
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    <Switch>
                        <Route path="/dashboard" component={DashboardContainer} />
                        <Route path="/integrations" component={IntegrationsContainer} />
                        <Route path="/locations" component={LocationsContainer} />
                        <Route path="/postings/details/:id" exact component={PostDetail} />
                        <Route path="/postings" component={Posting} />
                        <Route path="/applicants" component={ApplicantsContainer} />
                        <Route path="/employees" component={EmployeesContainer} />
                        <Route path="/users" component={UsersContainer} />
                        <Route path="/reports" component={ReportsContainer} />
                        <Route path="/organization" component={OrganizationContainer} />
                        <Route path="/orgsettings" component={OrgSettingsContainer} />
                        <Route path="/help" component={HelpContainer} />
                        <Route path="/account/profile" component={ProfileContainer} />
                        <Route path="/account/settings" component={SettingsContainer} />
                        <Route path="/resources" component={ResourcesContainer} />
                        <Route path="/login" component={SignInContainer} />
                        <Route path="/" component={DashboardContainer} />
                    </Switch>
                    <div className="copyright">Copyright {new Date().getFullYear()}, talentReef, Inc. All rights reserved.</div>
                </main>
            </div>
            <Dialog open={showConfig} onClose={() => { return toggleConfig()}} className={classes.portalDialog}>
                <div className={classes.portal}>
                    <h1>Platform Configuration</h1>
                    <div className="bottom-spacer-30">
                        <h3>Client Portal & Branding Configuration</h3>
                        <select className="custom-select" id="client">
                            <option value="0">Please select an option...</option>
                            <option value="0">talentReef</option>
                            <option value="1">TacoBell</option>
                            <option value="2">Boston Market</option>
                            <option value="3">Waste Management</option>
                        </select>
                    </div>
                    <div className="bottom-spacer-30">
                        <h3>Subscription Configuration</h3>
                        <FormControlLabel
                            control={
                                <Checkbox checked={applicantTracking} onChange={handleChange('applicanttracking')} value="applicanttracking" />
                            }
                            label="Applicant Tracking"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox checked={employeeManagement} onChange={handleChange('employeemanagement')} value="EmployeeManagement" />
                            }
                            label="Employee Management"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox checked={recruit} onChange={handleChange('recruit')} value="Recruit" />
                            }
                            label="Recruit 2.0"
                        />
                    </div>
                    <div className="bottom-spacer-30">
                        <h3>Impersonate Role</h3>
                        <select className="custom-select" id="role">
                            {
                                Roles.data.map((r) => {
                                    return (
                                            <option value={r.id}>{r.name}</option>
                                        )
                                })
                            }
                        </select>
                    </div>
                    <p>
                        <Button color="primary" variant="contained" title="Save Configuration" onClick={saveConfig}>Save Configuration</Button>
                    </p>
                </div>
            </Dialog>
            <Loading color="primary"
                     loaderStyle="Circular"
                     loaderType="Indeterminate"
                     loadingMessage={'Updating ...'}
                     size={250}
                     visible={loading}
                     withOverlay={true} />
            <SnackbarWrapper
                anchorOrigin={{ vertical: 'top', horizontal: 'center'}}
                autoHideDuration={6000}
                onClose={handleCloseSnack}
                open={notification.length > 0}
                color="success"
                message={notification}
            />
        </Router>
    )
};

export default Shell;