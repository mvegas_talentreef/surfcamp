import React from 'react';
import PageHeader from "../_base/PageHeader/PageHeader";

const Organization = () => {
    return (
        <PageHeader title="Organization Information" utilityComponent={null} />
    )
};

export default Organization;