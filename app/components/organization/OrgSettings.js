import React from 'react';
import PageHeader from "../_base/PageHeader/PageHeader";

const OrgSettings = () => {
    return (
        <PageHeader title="Organization Settings" utilityComponent={null} />
    )
};

export default OrgSettings;