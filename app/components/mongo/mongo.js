import React, {useState, useEffect} from 'react';
import Container from "reactstrap/es/Container";
import Paper from "@material-ui/core/Paper";

import { mongoClient } from '../../stitch/mongodb';
import LinearProgress from "@material-ui/core/LinearProgress";
import Row from "reactstrap/es/Row";

const Mongo = () => {

    const [results, setResults ] = useState(null);
    const [completed, setCompleted ] = useState(false);

    useEffect(() => {
        if (results == null) {
            function progress() {
                setCompleted(oldCompleted => {
                    if (oldCompleted === 100) {
                        return 0;
                    }
                    const diff = Math.random() * 10;
                    return Math.min(oldCompleted + diff, 100);
                });
            }
            const timer = setInterval(progress, 500);

            mongoClient
                .find({}, {limit: 50})
                .toArray()
                .then(docs => {
                    setResults(docs);
                    console.log('found docs', docs);
                    setCompleted(100);
                    clearInterval(timer);
                });
        }
    });

    return (
        <Container fluid>
            <Paper className={classes.paper}>
                <h2>MongoDB Integration</h2>
                <p>This is a demonstration of MongoDB and MongoDB Stitch. It is a serverless platform that enables developers to quickly build enterprise applications without having to setup server infrastructure. It is built on top of MongoDB Atlas, automatically integrating the connection to your database. Via Stitch, you can read and write data. It includes a secure, role-based rules engine to ensure your users can only read and change data you want them to. In addition, it simplifies the process of authenticating and managing your application's users.
                    It lets them browse the app anonymously and then we can link their behavior to them when they create an account.</p>
                <p>Lastly, it is very fast. I'm using a sample data collection, pulling about 40mb of data in about 1 second. This is without any performance tuning, no data filters, and just out-of-the-box functionality. Thus performance would be even better and in the milliseconds.</p>
                <h3>Sample Data Request of Listings and Reviews</h3>
                {
                    !results ?  <LinearProgress className={classes.progress} /> : results && results.map((l, i) => {
                        return (
                            <Row key={i} className={classes.rows}>
                                <strong>{l.name}</strong>
                                <p>{l.summary}</p>
                                <a href={l.listing_url} target={'_blank'}>{l.listing_url}</a>
                            </Row>
                        )
                    })
                }
            </Paper>
        </Container>
    );
};

export default Mongo;