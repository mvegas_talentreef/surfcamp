import React from 'react';
import PageHeader from "../_base/PageHeader/PageHeader";

const Employees = () => {
    return (
        <PageHeader title="Employees" utilityComponent={null} />
    )
};

export default Employees;