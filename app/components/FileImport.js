import React, { Fragment } from 'react';
import { getDateWithFormat } from '../helpers/Utils';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import {useDropzone} from 'react-dropzone';
import WarningIcon from '@material-ui/icons/Warning';
import { MdFileDownload} from "react-icons/all";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'block',
        padding: theme.spacing(3, 3),
        marginBottom: 12,
        color: '#777777'
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    content: {
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    actions: {
        textAlign: 'center',
        marginTop: 40
    },
    templateIcon: {
        fontSize: 2
    },
    footer: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        paddingBottom: 40
    },
}));

const FileImport = () => {

    function getSteps() {
        return ['Download Mass Hire Template', 'Upload Mass Hire Template', 'Confirm and Process'];
    }

    const stepOne = () => {
        return (
            <Fragment>
                <Button className="downloadButton">
                    <MdFileDownload className="download-icon" /> <h3>Download Mass Hire Template</h3>
                </Button>
                <div className="step-desc">Skip to step 2 if you're ready to upload your file...</div>
            </Fragment>
        )
    };

    const stepTwo = () => {
        return (
            <Fragment>
                <div {...getRootProps({className: 'dropzone'})}>
                    <input {...getInputProps()} />
                    <p>Drag 'n' drop your template file here, or click to select your file.</p>
                </div>
                {files}
                <p>{ 'For processing, your file will be renamed to: MinMax_' + getDateWithFormat() + '.csv' }</p>
            </Fragment>
        )
    };

    const stepThree = () => {
        return (
            <Fragment>
                <h3>Click Finish to confirm and process your file</h3>
                <div className={classes.actions}>
                        <div className="warning-bg">
                            <WarningIcon className="warning-icon" />
                            By submitting a value of 0.00 for both minimum and maximum pay rate, this will delete/reset the min/max payrate record for that position/location.
                        </div>
                </div>
            </Fragment>
        )
    };

    function getStepContent(stepIndex, classes) {
        switch (stepIndex) {
            case 0:
                return stepOne(classes);
            case 1:
                return stepTwo();
            case 2:
                return stepThree();
            default:
                return 'Uknown stepIndex';
        }
    }

    const classes = useStyles();

    // for the stepper component
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps();

    function handleNext() {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
    }

    function handleBack() {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    }

    function handleReset() {
        setActiveStep(0);
    }

    // for drop zone component
    const {acceptedFiles, getRootProps, getInputProps} = useDropzone();
    const files = acceptedFiles.map(file => (
        <div key={file.path}>
            {file.path} - {file.size} bytes
        </div>
    ));

    return (
        <div>
            <h2>Min/Max File Import Tool</h2>
            <Paper className={classes.root}>
                <main className={classes.content}>
                    <Stepper activeStep={activeStep} alternativeLabel>
                        {steps.map(label => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    <div>

                        {activeStep === steps.length ? (
                            <div>
                                <Typography className={classes.instructions}>All steps completed</Typography>
                                <Button onClick={handleReset}>Reset</Button>
                            </div>
                        ) : (
                            <Grid container className={classes.footer}>
                                <Grid item md={12} className={classes.actions}>
                                    {getStepContent(activeStep, classes)}
                                </Grid>
                                <Grid item md={12} className={classes.actions}>
                                    <Button
                                        disabled={activeStep === 0}
                                        onClick={handleBack}
                                        className={classes.backButton}
                                    >
                                        Back
                                    </Button>
                                    <Button variant="contained" color="secondary" onClick={handleNext}>
                                        {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                    </Button>
                                </Grid>
                            </Grid>
                        )}
                    </div>
                </main>
            </Paper>
        </div>
    );
};

export default FileImport;