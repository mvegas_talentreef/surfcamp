import React, { Fragment } from 'react';
import Paper from "@material-ui/core/Paper";
import { makeStyles } from '@material-ui/core/styles';
import {useDropzone} from 'react-dropzone';
import {getDateWithFormat} from "../helpers/Utils";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from "@material-ui/core/FormControlLabel";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'block',
        padding: theme.spacing(3, 3),
        marginBottom: 12,
        color: '#777777'
    },
    process: {
        backgroundColor: '#aace48',
        color: '#ffffff'
    },
    actions: {
        display: 'flex',
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignContent: 'center',
        textAlign: 'right'
    }
}));

const NewFileImport = () => {

    const [confirm, setConfirm] = React.useState(false);

    const classes = useStyles();

    const handleChange = name => event => {
        setConfirm(!confirm);
    };

    // for drop zone component
    const {acceptedFiles, getRootProps, getInputProps} = useDropzone();
    const files = acceptedFiles.map(file => (
        <div key={file.path}>
            {file.path} - {file.size} bytes
        </div>
    ));

    return (
        <Fragment>
            <Grid container>
                <Grid item xs={6}>
                    <h2>Marketing Assets</h2>
                </Grid>
                <Grid item xs={6} className={classes.actions}>
                    <Button variant="contained" color="secondary">Download Mass Import Template</Button>
                </Grid>
            </Grid>
            <Paper className={classes.root}>
                <div {...getRootProps({className: 'dropzone'})}>
                    <input {...getInputProps()} />
                    <p>Drag 'n' drop your template file here, or click to select your file.</p>
                </div>
                <p>
                    {files}
                </p>
                <p>{ 'For processing, your file will be renamed to: MinMax_' + getDateWithFormat() + '.csv' }</p>
                <FormControlLabel
                    control={<Checkbox checked={confirm} onChange={handleChange('confirm')} value="confirm" />}
                    label="I understand by submitting a value of 0.00 for both minimum and maximum pay rate, this will delete/reset the min/max payrate record for that position/location."
                />
                <p>
                    <Button variant="contained" color="secondary" disabled={!confirm}>Import and Process</Button>
                </p>
            </Paper>
        </Fragment>
    )
};

export default NewFileImport;