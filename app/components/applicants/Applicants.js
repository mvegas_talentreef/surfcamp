import React, {Fragment} from 'react';
import { makeStyles  } from '@material-ui/core/styles';
import PageHeader from "../_base/PageHeader/PageHeader";
import TwoColumnLayout from "../_base/TwoColumnLayout/TwoColumnLayout";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(theme => ({
    root: {
        flex: 1,
        display: 'flex',

    },
    paper: {
        padding: '24px',
        paddingTop: '12px',
        margin: '12px',
    }
}));

const SideBarContent = () => {
    const classes = useStyles();
    return (
        <Paper className={classes.paper}>
            <h4>Sidebar</h4>
            <p>Sample sidebar content, blah blah blah</p>
        </Paper>
    )
};

const Applicants = () => {
    const classes = useStyles();

    return (
        <Fragment>
            <PageHeader title="Applicants" utilityComponent={null} />
            <TwoColumnLayout sidebar={<SideBarContent  />}>
                <Paper className={classes.paper}>
                    <h4>Blah blah blah</h4>
                    <p>Main body content goes here</p>
                </Paper>
            </TwoColumnLayout>
        </Fragment>
    )
};

export default Applicants;