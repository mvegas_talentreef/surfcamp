import React from 'react';
import PageHeader from "../_base/PageHeader/PageHeader";

const Settings = () => {
    return (
        <PageHeader title="My Settings" utilityComponent={null} />
    )
};

export default Settings;