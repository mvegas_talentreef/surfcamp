import React  from 'react';
import '../../assets/styles/index.scss';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import {makeStyles } from '@material-ui/core/styles';
import firebase from 'firebase';
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(theme => ({
    root: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        marginTop: 40
    },
    frame: {
      width: 400,
      padding: '40px',
        textAlign: 'center'
    },
    subheading: {
        color: '#00A2C2'
    }
}));

// Configure FirebaseUI.
const uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
    signInSuccessUrl: '/',
    // We will display Google and Facebook as auth providers.
    signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.FacebookAuthProvider.PROVIDER_ID
    ]
};

const SignIn = () => {
    const classes = useStyles();

        return (
            <Container className={classes.root}>
                <Paper className={classes.frame}>
                    <h1>Welcome to talentReef</h1>
                    <h3 className={classes.subheading}>Please sign in with one of the following:</h3>
                    <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()}/>
                </Paper>
            </Container>
        );
};

export default SignIn;