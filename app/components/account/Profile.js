import React, {Fragment, useContext} from 'react';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import Avatar from 'react-avatar';
import Grid from "@material-ui/core/Grid";
import { UserConsumer } from "../../store/store";
import PageHeader from "../_base/PageHeader/PageHeader";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import OneColumnLayout from "../_base/OneColumnLayout/OneColumnLayout";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from '@material-ui/core/TableCell';
import Button from "@material-ui/core/Button";
import { LoadingContext } from "../../store/store";
import CardActions from "@material-ui/core/CardActions";

const useStyles = makeStyles(theme => ({
    cardMedia: {
        margin: 0,
        alignContent: 'space-evenly',
        justifyContent: 'space-evenly'
    },
    cards: {
        margin: '12px',
        padding: '12px'
    },
    displayName: {
      fontSize: 'medium'
    },
    title: {
        color: '#999999',
        fontSize: 'small'
    },
    profileLead: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        marginBottom: '24px',
    },
    aside: {
        borderRight: 'solid 1px #cccccc',
        fontSize: 'small',
    },
    section: {
        paddingTop: '6px',
        paddingBottom: '12px',
        paddingLeft: '12px'
    },
    rightCol: {
        paddingLeft: '24px'
    },
    avatar: {
        marginRight: '20px'
    },
    appbar: {
        radius: 4
    },
    label: {
        marginTop: '24px',
        padding: '12px',
        backgroundColor: '#f2f2f2',
        borderRadius: '4px'
    },
    table: {
        fontSize: 'small',
        paddingLeft: '12px',
        color: '#777777'
    },
    row: {
        display: 'flex',
        flexWrap: 'wrap',
        marginLeft: '12px'
    },
    indicator: {
        color: '#33cc99',
        backgroundColor: '#33cc99'
    }
}));

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            <Box p={3}>{children}</Box>
        </Typography>
    );
}

// const StyledTableCell = makeStyles(theme => ({
//     head: {
//         backgroundColor: theme.palette.common.black,
//         color: theme.palette.common.white,
//     },
//     body: {
//         fontSize: 14,
//     },
// }))(TableCell);
//
// const StyledTableRow = makeStyles(theme => ({
//     root: {
//         '&:nth-of-type(odd)': {
//             backgroundColor: theme.palette.background.default,
//         },
//     },
// }))(TableRow);

function createData(name, location, from, to, degree) {
    return { name, location, from, to, degree };
}

const rows = [
    createData('Commerce City High School', 'Commerce City', '04/2017', "09/2017", "On Hold"),
    createData('Decatur High School', "Denver", "03/2016", "11/2016", "Still Working"),
    createData('Denver Technical University', "Denver", "07/2012", "11/2015", "BA"),
    createData('Houston Night School', "Houston", "03/2011", "07/2012", "Diploma"),
];

const Profile = () => {

    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const [loading, setLoading] = useContext(LoadingContext);

    const updateProfile = () => {
        setLoading(true),
            setTimeout(() => {
                setLoading(false);
            }, 3000);
    };

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
            <UserConsumer>
                { ([user, ])  => (
                    <Fragment>
                        <PageHeader title="My Account" utilityComponent={null} />
                            <OneColumnLayout>
                                <Paper className={classes.appbar}>
                                    <AppBar color="primary" position="static">
                                        <Tabs
                                            value={value}
                                            classes={{indicator: classes.indicator}}
                                            onChange={handleChange}
                                            aria-label="disabled tabs example"
                                        >
                                            <Tab label="My Information" />
                                            <Tab label="My Tasks" />
                                            <Tab label="My Applications" />
                                            <Tab label="My Locations" />
                                        </Tabs>
                                    </AppBar>
                                    <TabPanel value={value} index={0}>
                                        <Grid container>
                                            <Grid item md={3} className={classes.aside}>
                                                <div className={classes.profileLead}>
                                                    <Avatar className={classes.avatar} name={user ? user.displayName : 'Guest'} color="#33cc99" round={true} />
                                                    { user &&
                                                        <h4 className={classes.displayName}>
                                                            { user.displayName }
                                                            <div className={classes.title}>Site Administrator</div>
                                                        </h4>
                                                    }
                                                </div>
                                                <div className={classes.section}>
                                                    <div className="col-6">
                                                       Phone:
                                                    </div>
                                                    <div className="col-6">
                                                        720.334.3838
                                                    </div>
                                                </div>
                                                <div className={classes.section}>
                                                    <div className="col-6">
                                                        Email Address:
                                                    </div>
                                                    <div className="col-6">
                                                        { user && user.email }
                                                    </div>
                                                </div>
                                            </Grid>
                                            <Grid item md={9} className={classes.rightCol}>
                                                <h4 className={classes.label}>Personal Information</h4>
                                                <div className={classes.row}>
                                                    <div className="col-md-3">
                                                        Address:
                                                    </div>
                                                    <div className="col-md-5">
                                                        5365 West 73rd Place
                                                    </div>
                                                </div>
                                                <div className={classes.row}>
                                                    <div className="col-md-3">
                                                        City:
                                                    </div>
                                                    <div className="col-md-5">
                                                       Westminster
                                                    </div>
                                                </div>
                                                <div className={classes.row}>
                                                    <div className="col-md-3">
                                                        State:
                                                    </div>
                                                    <div className="col-md-5">
                                                        Colorado
                                                    </div>
                                                </div>
                                                <div className={classes.row}>
                                                    <div className="col-md-3">
                                                        Zip:
                                                    </div>
                                                    <div className="col-md-5">
                                                        80003
                                                    </div>
                                                </div>
                                                <h4 className={classes.label}>Education</h4>
                                                <Table classes={{table: classes.table}} aria-label="customized table">
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell>Name</TableCell>
                                                            <TableCell align="right">Location</TableCell>
                                                            <TableCell align="right">From</TableCell>
                                                            <TableCell align="right">To</TableCell>
                                                            <TableCell align="right">Degree</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody>
                                                        {rows.map(row => (
                                                            <TableRow key={row.name}>
                                                                <TableCell component="th" scope="row">
                                                                    {row.name}
                                                                </TableCell>
                                                                <TableCell align="right">{row.location}</TableCell>
                                                                <TableCell align="right">{row.from}</TableCell>
                                                                <TableCell align="right">{row.to}</TableCell>
                                                                <TableCell align="right">{row.degree}</TableCell>
                                                            </TableRow>
                                                        ))}
                                                    </TableBody>
                                                </Table>
                                                <h4 className={classes.label}>Employment</h4>
                                                <h4 className={classes.label}>References</h4>
                                                <Button variant="contained" color="secondary" onClick={updateProfile}>Learn more</Button>
                                            </Grid>
                                        </Grid>
                                    </TabPanel>
                                </Paper>
                            </OneColumnLayout>
                    </Fragment>
                )}
            </UserConsumer>
    )
};

export default Profile;