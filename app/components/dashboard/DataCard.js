import React, {Fragment} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from "@material-ui/core/Card";

const useStyles = makeStyles(theme => ({
    card: {
        flex: 1,
        flexDirection: 'column',
        display: 'flex',
        alignContent: 'center',
        alignItems: 'center',
        width: '30%',
        padding: '40px',
        textAlign: 'center',
        margin: '6px',
        h4: {
            margin: 0
        },
    },
    count: {
        fontSize: '36px',
        opacity: '0.8',
        fontWeight: 600,
        color: '#00a2c2'
    }
}));

const DataCard = (props) => {
    const classes = useStyles();
    return (
        <Card className={classes.card}>
            <div className={classes.count}>{props.total}</div>
            <h4>{props.title}</h4>
        </Card>
    )
};

export default DataCard;