import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
    card: {
        flex: 1,
        flexDirection: 'column',
        display: 'flex',
        alignContent: 'center',
        alignItems: 'center',
        width: '30%',
        padding: '40px',
        textAlign: 'center',
        margin: '6px',
        h4: {
            margin: 0
        },
    }
}));

const SummaryCard = (props) => {
    const classes = useStyles();
    return (
        <Card className={classes.table}>
            <CardHeader title={props.title} className={classes.tableHeader} />
            <CardContent>
                {props.content}
            </CardContent>
            <CardActions>
                <Button variant="contained" color="primary">Learn more</Button>
            </CardActions>
        </Card>
    )
};

export default SummaryCard;