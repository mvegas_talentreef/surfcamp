import React, {Fragment, useContext, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Row from "reactstrap/es/Row";
import Container from "reactstrap/es/Container";
import Card from "@material-ui/core/Card";
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from "@material-ui/core/Button";
import logo from '../../assets/images/1280px-E-Verify_logo.svg.png';
import PageHeader from "../_base/PageHeader/PageHeader";
import { LoadingContext } from "../../store/store";

// charts
import CanvasJSReact from '../../assets/scripts/canvasjs.react';
let CanvasJSChart = CanvasJSReact.CanvasJSChart;

import Loading from "../_base/Loading/Loading";

const useStyles = makeStyles(theme => ({
    root: {
        flex: 1,
        display: 'flex',
        padding: theme.spacing(2, 4),
        marginBottom: 12,
        color: '#777777',
    },
    generic: {
        flex: 1,
        display: 'block',
        padding: theme.spacing(2, 4),
        marginBottom: 12,
        color: '#777777',
    },
    input: {
        margin: theme.spacing(1),
    },
    row: {
        flexDirection: 'row',
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    },
    cards: {
        flex: 1,
        display: 'flex',
        alignContent: 'space-evenly',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    table: {
        flex: 1,
        flexDirection: 'column',
        display: 'flex',
        alignContent: 'flex-start',
        alignItems: 'flex-start',
        justifyContent: 'center',
        padding: '20px',
        textAlign: 'left',
        margin: '6px',
        color: '#777777',
        h4: {
            margin: 0
        }
    },
    paper: {
      flex: 1,
      flexDirection: 'column',
      padding: '40px',
        margin: '6px'
    },
    tableHeader: {
        backgroundColor: '#e2e2e2',
        color: '#666666',
        borderRadius: 4,
        width: '100%',
    },
    card: {
        flex: 1,
        flexDirection: 'column',
        display: 'flex',
        alignContent: 'center',
        alignItems: 'center',
        width: '30%',
        padding: '40px',
        textAlign: 'center',
        margin: '6px',
        h4: {
            margin: 0
        },
    },
    count: {
        fontSize: '36px',
        opacity: '0.8',
        fontWeight: 600,
        color: '#00a2c2'
    },
    logo: {
        width: 150,
        marginBottom: '24px'
    },
    progress: {
        marginTop: 40,
        marginBottom: 40,
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
    rows: {
        borderTop: 'solid #eeeeee 1px',
        paddingTop: '12px',
        paddingBottom: '12px',
        paddingLeft: '24px',
        paddingRight: '24px'
    }
}));

const Dashboard = () => {

    const [loading, setLoading] = useContext(LoadingContext);
    const [backgroundLoader, setBackgroundLoader] = useState(false);
    const classes = useStyles();

    const updateList = () => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false)
        }, 3000);
    };

    const updateBackgroundChecks = () => {
        setBackgroundLoader(true);
        setTimeout(() => {
            setBackgroundLoader(false);
        }, 3000)
    };

    const options = {
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: ""
        },
        width: '400',
        height: '200',
        data: [{
            type: "pie",
            startAngle: 75,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}",
            dataPoints: [
                { y: 18, label: "Cashier" },
                { y: 49, label: "Dish Washer" },
                { y: 9, label: "Cook" },
                { y: 5, label: "Receptionist" },
                { y: 19, label: "Other" }
            ]
        }]
    };

    const satisfactions = {
        animationEnabled: true,
        title: {
            text: ""
        },
        subtitles: [{
            text: "71% +",
            verticalAlign: "center",
            fontSize: 24,
            dockInsidePlotArea: true
        }],
        width: '400',
        height: '200',
        data: [{
            type: "doughnut",
            showInLegend: true,
            indexLabel: "{name}: {y}",
            yValueFormatString: "#,###'%'",
            dataPoints: [
                { name: "Unsatisfied", y: 5 },
                { name: "Very Unsatisfied", y: 31 },
                { name: "Very Satisfied", y: 40 },
                { name: "Satisfied", y: 17 },
                { name: "Neutral", y: 7 }
            ]
        }]
    };

    return (
        <Fragment>
            <PageHeader title="Dashboard" utilityComponent={null} />
            <Container fluid>
                <Row className={classes.cards}>
                    <Card className={classes.card}>
                        <h2>New Applicants</h2>
                        <CanvasJSChart options = {options} />
                    </Card>
                    <Card className={classes.card}>
                        <h2>Customer Satisfaction</h2>
                        <CanvasJSChart options = {satisfactions} />
                    </Card>
                </Row>
            </Container>
            <Container fluid>
                <Row className={classes.cards}>
                    <Card className={classes.table}>
                        <CardContent>
                            <img src={logo} className={classes.logo} alt={''} />
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet luctus odio, in rhoncus nulla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi viverra est sed justo ultricies blandit. Fusce gravida, arcu at facilisis pulvinar, nulla justo finibus purus, in placerat quam augue quis sapien.
                            </p>
                            </CardContent>
                        <CardActions>
                            <Button variant="contained" color="primary" onClick={updateList}>Learn more</Button>
                        </CardActions>
                    </Card>
                    <Card className={classes.table}>
                        {
                            !backgroundLoader &&
                                <Fragment>
                                    <CardHeader title="Background Checks" className={classes.tableHeader} />
                                    <CardContent>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet luctus odio, in rhoncus nulla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi viverra est sed justo ultricies blandit. Fusce gravida, arcu at facilisis pulvinar, nulla justo finibus purus, in placerat quam augue quis sapien.
                                                </p>
                                    </CardContent>
                                </Fragment>
                        }
                        <Loading
                            loaderStyle="Circular"
                            color="secondary"
                            loaderType="Indeterminate"
                            loadingMessage={'Updating background checks...'}
                            size={100}
                            visible={backgroundLoader}
                            withOverlay={false}
                        />
                        <CardActions>
                            <Button variant="contained" color="secondary" onClick={updateBackgroundChecks}>Learn more</Button>
                        </CardActions>
                    </Card>
                </Row>
            </Container>
        </Fragment>
    )
};

export default Dashboard;