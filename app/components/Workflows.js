import React, { useState, useCallback } from 'react';
import Card from './WorkflowCard';
import update from 'immutability-helper'
const style = {
    width: 400,
};

const Workflows = () => {
    {
        const [cards, setCards] = useState([
            {
                id: 1,
                text: 'Applicant applied, send email notification to hiring manager/recruiter',
            },
            {
                id: 2,
                text: 'Send invite for assessment if passed criteria',
            },
            {
                id: 3,
                text: 'Automatically setup interview if passed criteria',
            },
            {
                id: 4,
                text: 'Send invite to candidate for available interview days and times',
            },
            {
                id: 5,
                text:
                    'Send email notifications to hiring manager/recruiter if applicant status changes',
            },
            {
                id: 6,
                text: 'Assessment completed and passed',
            },

        ]);
        const moveCard = useCallback(
            (dragIndex, hoverIndex) => {
                const dragCard = cards[dragIndex]
                setCards(
                    update(cards, {
                        $splice: [
                            [dragIndex, 1],
                            [hoverIndex, 0, dragCard],
                        ],
                    }),
                )
            },
            [cards],
        );

        const renderCard = (card, index) => {
            return (
                <Card
                    key={card.id}
                    index={index}
                    id={card.id}
                    text={card.text}
                    moveCard={moveCard}
                />
            )
        }
        return (
            <>
                <div style={style}>{cards.map((card, i) => renderCard(card, i))}</div>
            </>
        )
    }
};

export default Workflows;
