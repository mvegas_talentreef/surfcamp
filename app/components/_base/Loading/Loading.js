import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import CircularProgress from "@material-ui/core/CircularProgress";
import LinearProgress from "@material-ui/core/LinearProgress";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    overlay: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: 10000,
        backgroundColor: '#000000',
        opacity: 0.8,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        height: '100vh',
        width: '100vw',
    },
    inner: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        opacity: 1,
        zIndex: 1000,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
    },
    wrapper: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: 10000,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        opacity: 1,
        height: '100vh',
        width: '100vw',
        color: '#ffffff',
        fontSize: 'large'
    },
    inline: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        zIndex: 10000,
        opacity: 1,
        margin: 0,
        padding: 0,
        height: '200px',
        width: '200px',
        color: '#777777',
        fontSize: 'small'
    },
    message: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'center',
        height: '100vh',
        width: '100vw',
        zIndex: 100000,
        opacity: 1,
        color: 'inherit',
        marginTop: '24px',
        fontSize: 'inherit'
    },
    loader: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        width: '100vw',
        zIndex: 100000,
        opacity: 1,
    },
    linear: {
        height: '20px',
        width: '90vw'
    }
}));

const Loading = (props) => {

    const { loaderStyle, loaderType, color, withOverlay, size, visible, loadingMessage } = props;
    const classes = useStyles();

    const LoadingContents = () => {
        return (
            <Fragment>
                <div className={classes.inner}>
                    {
                        loaderStyle.toString().toLowerCase() === 'circular' &&
                        <CircularProgress className={classes.loader} size={size} variant={loaderType.toString().toLowerCase()} color={color} value={100} />
                    }
                    {
                        loaderStyle.toString().toLowerCase() === 'linear' &&
                        <LinearProgress className={classes.linear} variant={loaderType.toString().toLowerCase()} color={color} value={100} />
                    }
                </div>
                <div className={classes.message}>
                    {
                        loadingMessage
                    }
                </div>
            </Fragment>
        )
    };

    return (
        <Fragment>
            {
                visible &&
                    <Fragment>
                        {
                            withOverlay &&
                                <Fragment>
                                    <div className={classes.overlay} />
                                    <div className={classes.wrapper}>
                                        <LoadingContents />
                                    </div>
                                </Fragment>
                        }
                        {
                            !withOverlay &&
                                <Fragment>
                                    <div className={classes.inline}>
                                        <LoadingContents />
                                    </div>
                                </Fragment>
                        }
                    </Fragment>
            }
        </Fragment>
    );
};

Loading.propTypes = {
    loaderStyle: PropTypes.oneOf(['Circular', 'Linear']),
    loaderType: PropTypes.oneOf(['Determinate', 'Indeterminate']),
    color: PropTypes.oneOf(['primary', 'secondary']),
    withOverlay: PropTypes.bool,
    size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    loadingMessage: PropTypes.string,
    visible: PropTypes.bool
};

export default Loading;