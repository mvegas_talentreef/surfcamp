import React from 'react';
import ListWrapper from "./ListWrapper";
import ListItemWrapper from "./ListItemWrapper";
import { makeStyles } from '@material-ui/core/styles';
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(theme => ({
    root: {
        flex: 1,
        display: 'flex',
        padding: '20px',
        flexDisplay: 'row',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center'
    }
}));

const ListContainer = (props) => {
    const classes = useStyles();
    const { listContent } = props;
    return (
        <Paper className={classes.root}>
            <ListWrapper>
                {
                    listContent.data.map((template) => (
                        <ListItemWrapper alignItems={'flex-start'} key={template.id} detailPath={template.detailPath + template.id} children={
                                <h2>{template.label}</h2>
                        } />
                    ))
                }
            </ListWrapper>
        </Paper>
    );
};

export default ListContainer;