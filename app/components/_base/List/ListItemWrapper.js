import React from 'react';
import ListItem from "@material-ui/core/ListItem";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

const ListItemWrapper = props => {
    const { alignItems, detailPath, children } = props;
    return (
        <Link to={detailPath}>
            <ListItem alignItems={alignItems} button>
                {children}
            </ListItem>
        </Link>
    );
};

ListItemWrapper.propTypes = {
    alignItems: PropTypes.string,
    detailPath: PropTypes.string,
    children: PropTypes.element.isRequired,
};

export default ListItemWrapper;