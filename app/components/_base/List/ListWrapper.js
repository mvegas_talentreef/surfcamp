import React from 'react';
import List from '@material-ui/core/List';

const ListWrapper = props => {
    const { children } = props;
    return (
        <List>
            {children}
        </List>
    );
};

export default ListWrapper;