import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import PropTypes from 'prop-types';
import ListItemWrapper from "../List/ListItemWrapper";

const useStyles = makeStyles(theme => ({
    actions: {
        textAlign: 'right'
    }
}));

const PageHeader = (props) => {

    const classes = useStyles();
    const { title, utilityComponent } = props;

    return (
        <Grid container>
            <Grid item xs={6}>
                <h2>{title}</h2>
            </Grid>
            <Grid item xs={6} className={classes.actions}>
                {utilityComponent}
            </Grid>
        </Grid>
    )
};

PageHeader.propTypes = {
    title: PropTypes.string.isRequired,
    utilityComponent: PropTypes.element
};

export default PageHeader;