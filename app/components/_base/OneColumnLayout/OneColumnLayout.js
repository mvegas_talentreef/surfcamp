import React from 'react';
import Grid from "@material-ui/core/Grid";

const OneColumnLayout = (props) => {
    const { children } = props;
    return (
        <Grid container>
            <Grid item xs={12} md={12}>
                {children}
            </Grid>
        </Grid>
    )
};

export default OneColumnLayout;