## One Column Layout

### Props
- children (required)

One column layout. Wrapper for one column page views.

View 'Show Info' for implementation details.