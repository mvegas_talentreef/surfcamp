import React from 'react';
import Grid from "@material-ui/core/Grid";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'flex-start',
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    }
}));

const TwoColumnLayout = (props) => {
    const classes = useStyles();

    const { sidebar, children } = props;
    return (
        <Grid container className={classes.root}>
            <Grid item xs={4} md={4}>
                {sidebar}
            </Grid>
            <Grid item xs={8} md={8}>
                {children}
            </Grid>
        </Grid>
    )
};

export default TwoColumnLayout;