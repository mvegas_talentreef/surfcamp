import React from 'react';
import PropTypes from 'prop-types';
import CanvasJSReact from "../../../assets/scripts/canvasjs.react";
let CanvasJSChart = CanvasJSReact.CanvasJSChart;

const Chart = props => {
    const data = {
        animationEnabled: props.animationEnabled,
        title: props.title,
        subTitle: props.subTitle,
        height: props.height,
        width: props.width,
        chartData: props.chartData
    };

    return (
        <CanvasJSChart children={data} />
    );
};

Chart.propTypes = {
    animationEnabled: PropTypes.bool,
    title: PropTypes.string,
    subTitle: PropTypes.string,
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    chartData: PropTypes.object
};

export default Chart;