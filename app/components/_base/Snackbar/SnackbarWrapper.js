import React, {useContext} from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import {NotificationContext} from "../../../store/store";

const SnackbarWrapper = (props) => {
    const { anchorOrigin, autoHideDuration, children, key, message, onClose, onEnter, onEntered, onEntering, onExit, onExited, onExiting, open } = props;
    const [notification, setNotification] = useContext(NotificationContext);

    const OnClose = () => {
        setNotification('');
    };

    return (
        <Snackbar
            open={open}
            onClose={onClose}
            ContentProps={{
                'aria-describedby': 'message-id',
            }}
            message={message}
            children={children && children}
            autoHideDuration={autoHideDuration}
            anchorOrigin={anchorOrigin}
            action={
                <IconButton
                    key={key}
                    aria-label="Close"
                    color="inherit"
                    onClick={OnClose}
                >
                    <CloseIcon />
                </IconButton>
            }
            onEnter={onEnter && onEnter}
            onEntered={onEntered && onEntered}
            onEntering={onEntering && onEntering}
            onExit={onExit && onExit}
            onExited={onExited && onExited}
            onExiting={onExiting && onExiting}
        />
    );
};

SnackbarWrapper.propTypes = {
    anchorOrigin: PropTypes.shape({ vertical: PropTypes.oneOf(['top', 'bottom']), horizontal: PropTypes.oneOf(['center', 'left'])}),
    resumeHideDuration: PropTypes.number,
    autoHideDuration: PropTypes.number,
    children: PropTypes.object,
    key: PropTypes.any,
    message: PropTypes.string,
    onClose: PropTypes.func.isRequired,
    onEnter: PropTypes.func,
    onEntered: PropTypes.func,
    onEntering: PropTypes.func,
    onExit: PropTypes.func,
    onExited: PropTypes.func,
    onExiting: PropTypes.func,
    open: PropTypes.bool.isRequired,
    color: PropTypes.oneOf(['success', 'warning', 'alert', 'utility'])
};

export default SnackbarWrapper;