import React, {Fragment, useContext} from 'react';
import BusinessIcon from '@material-ui/icons/Business';
import ApplicantIcon from '@material-ui/icons/EmojiPeople';
import EmployeesIcon from '@material-ui/icons/Group';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import TuneIcon from '@material-ui/icons/Tune';
import StoreMallDirectoryIcon from '@material-ui/icons/StoreMallDirectory';
import HelpIcon from '@material-ui/icons/HelpOutline';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import DashboardIcon from '@material-ui/icons/Dashboard';
import StorefrontIcon from '@material-ui/icons/Storefront';
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Divider from "@material-ui/core/Divider";
import Tooltip from '@material-ui/core/Tooltip';
import {Link} from "react-router-dom";
import * as routes from '../../config/routes';
import ListItemText from "@material-ui/core/ListItemText";
import {SettingsContext} from "../../store/store";

const getIcon = (title) => {
  switch(title.toLowerCase()){
      case 'dashboard':
          return <DashboardIcon />;
      case 'locations':
          return <StoreMallDirectoryIcon />;
      case 'postings':
          return <PersonPinIcon />;
      case 'applicants':
          return <ApplicantIcon />;
      case 'employees':
          return <EmployeesIcon />;
      case 'users':
          return <AssignmentIndIcon />;
      case 'reports':
          return <AssignmentTurnedInIcon />;
      case 'settings':
          return <TuneIcon />;
      case 'store':
          return <StorefrontIcon />;
      case 'help':
          return <HelpIcon />;
  }
};

const SideBar = () => {
    const [ showConfig , setShowConfig ] = useContext(SettingsContext);

    const toggleSettings = () => {
        setShowConfig(!showConfig)
    };

    return (
        <Fragment>
            {
                routes.data.map(link => {
                    if (link.id === 666666) {
                        return (
                            <Divider className="divider" key={link.id} />
                        )
                    } else {
                            if (link.title.toLowerCase() === 'settings') {
                                return (
                                        <ListItem button key={link.title} onClick={toggleSettings}>
                                            <Tooltip title={link.title} placement="right">
                                                <ListItemIcon>
                                                    {getIcon(link.title)}
                                                </ListItemIcon>
                                            </Tooltip>
                                            <ListItemText>
                                                {link.title}
                                            </ListItemText>
                                        </ListItem>
                                )
                            } else {
                                return (
                                    <Link to={link.to} key={link.id}>
                                        <ListItem button key={link.title}>
                                            <Tooltip title={link.title} placement="right">
                                                <ListItemIcon>
                                                    {getIcon(link.title)}
                                                </ListItemIcon>
                                            </Tooltip>
                                            <ListItemText>
                                                {link.title}
                                            </ListItemText>
                                        </ListItem>
                                    </Link>
                                )
                            }
                    }
                })
            }
        </Fragment>
    )
};

export default SideBar;