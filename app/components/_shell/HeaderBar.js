import React, { useState, useContext, useEffect, Fragment } from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import NotificationsIcon from '@material-ui/icons/Notifications';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import FolderSpecialIcon from '@material-ui/icons/FolderSpecial';
import SettingsIcon from '@material-ui/icons/Settings';
import HelpIcon from '@material-ui/icons/Help';
import UserIcon from '@material-ui/icons/AccountCircle';
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Badge from "@material-ui/core/Badge";
import {Link} from "react-router-dom";
import Avatar from 'react-avatar';
import {BrandContext, UserContext, NotificationContext, LoadingContext} from "../../store/store";
import firebaseConfig from "../../config/firebase";
import { withRouter } from "react-router-dom";
import AppsIcon from '@material-ui/icons/Apps';

const useStyles = makeStyles(theme => ({
    title: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        flexGrow: 1,
        fontWeight: 600,
        img: {
            height: 40,
            padding: 20
        },
        icons: {
            backgroundColor: '#00a2c2',
            color: '#ffffff'
        },
        toolbar: {
          width: '100%'
        },
        search: {
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(1),
                width: 'auto',
            },
        },
        searchIcon: {
            width: theme.spacing(7),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        inputRoot: {
            color: 'inherit',
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 7),
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                width: 120,
                '&:focus': {
                    width: 200,
                },
            },
        },
        logo: {
            height: '60px'
        },
        appTitle: {
            fontSize: '18px !important',
            fontWeight: 100,
            heavy: {
                fontWeight: 600
            }
        },
    },
    snack: {
        backgroundColor: '#B3D84D',
        fontSize: 'large',
        color: '#ffffff'
    },
    displayName: {
        marginLeft: '6px'
    },
}));

const HeaderBar = (props) => {
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = useState(null);
    const [appAnchorEl, setAppAnchorEl] = useState(null);
    const [displayName, setDisplayName] = useState();
    const [photoUrl, setPhotoUrl] = useState();
    const [, setEmail] = useState();
    const [, setUid] = useState();
    const open = Boolean(anchorEl);
    const openAppMenu = Boolean(appAnchorEl);
    const [signedOut, setSignedOut] = useState(false);
    const [brandSettings,] = useContext(BrandContext);
    const [, setUser] = useContext(UserContext);
    const [,setNotification] = useContext(NotificationContext);
    const [loading, setLoading] = useContext(LoadingContext);

    useEffect(() => {
        firebaseConfig.auth().onAuthStateChanged(function(userObj) {
            if (userObj) {
                // User is signed in.
                setDisplayName(userObj.displayName);
                setEmail(userObj.emailVerified);
                setUid(userObj.uid);
                setPhotoUrl(userObj.photoURL);
                setUser(userObj);
                setSignedOut(false);
            }
        });
    });

    const handleAppMenu = (event) => {
        setAppAnchorEl(event.currentTarget);
    };

    const handleMenu = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
        //toggleDrawer(false)
    };

    const handleCloseAppMenu = () => {
        setAppAnchorEl(null);
    };

    const signOut = () => {
        setLoading(true);
        firebaseConfig.auth().signOut().then(function() {
            // Sign-out successful.
            setUser(null);
            setDisplayName(null);
            setSignedOut(true);
            setTimeout(() => {
                setLoading(false);
                setNotification('You have successfully signed out!');
            }, 3000);
        }).catch(function(error) {
            // An error happened.
        }).finally(() => {
                props.history.push('/');
            }
        );
    };

    return (
        <Fragment>
                    <div className={classes.title}>
                        <div>
                            <div className='appTitle'>
                                <Button className="text-white" onClick={handleAppMenu}>
                                    <AppsIcon className="white" />&nbsp;{brandSettings.displayName}
                                </Button>
                            </div>
                        </div>
                    </div>

                <Button color="inherit">
                    <Badge className={classes.margin} badgeContent={12} color="primary">
                        <NotificationsIcon className="white" />
                    </Badge>
                </Button>
            {
                displayName &&
                <Button color="inherit" onClick={handleMenu}><Avatar size="40px" src={photoUrl} color="#33cc99" round={true} />&nbsp;
                    <div className={classes.displayName}>{displayName}</div>
                </Button>
            }
            {
                displayName ?
                    <Button onClick={signOut} color="inherit"><ExitToAppIcon className="white" />&nbsp;Sign out</Button>
                    : <Link to="/login"><Button color="inherit"><LockOutlinedIcon className="white" />&nbsp;Sign in</Button></Link>

            }
                <Menu
                    id="menu-userbar"
                    anchorEl={appAnchorEl}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    keepMounted
                    transformOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    open={openAppMenu}
                    onClose={handleCloseAppMenu}
                >
                    <div className="app-menu">
                        <Link onClick={handleClose} to='/account/profile'>
                            <MenuItem>
                                <ListItemText primary="Applicant Tracking" />
                            </MenuItem>
                        </Link>
                        <Link onClick={handleClose} to='/resources'>
                            <MenuItem>
                                <ListItemText primary="Employee Management" />
                            </MenuItem>
                        </Link>
                        <Link onClick={handleClose} to='/account/settings'>
                            <MenuItem>
                                <ListItemText primary="Performance Management" />
                            </MenuItem>
                        </Link>
                        <Link onClick={handleClose} to='/account/settings'>
                            <MenuItem>
                                <ListItemText primary="Recruit 3.0" />
                            </MenuItem>
                        </Link>
                    </div>
                </Menu>
            <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={open}
                onClose={handleClose}
            >
                <div className="custom-menu">
                    <Link onClick={handleClose} to='/account/profile'>
                        <MenuItem>
                            <ListItemIcon>
                                <UserIcon className={classes.icons} />
                            </ListItemIcon>
                            <ListItemText primary="My Account Info" />
                        </MenuItem>
                    </Link>
                    <Link onClick={handleClose} to='/resources'>
                        <MenuItem>
                            <ListItemIcon>
                                <FolderSpecialIcon className={classes.icons} />
                            </ListItemIcon>
                            <ListItemText primary="Resources" />
                        </MenuItem>
                    </Link>
                    <Link onClick={handleClose} to='/account/settings'>
                        <MenuItem>
                            <ListItemIcon>
                                <SettingsIcon className={classes.icons} />
                            </ListItemIcon>
                            <ListItemText primary="Settings" />
                        </MenuItem>
                    </Link>
                    <Link onClick={handleClose} to='/help'>
                        <MenuItem>
                            <ListItemIcon>
                                <HelpIcon className={classes.icons} />
                            </ListItemIcon>
                            <ListItemText primary="Help" />
                        </MenuItem>
                    </Link>
                </div>
            </Menu>
        </Fragment>
    )
};

export default withRouter(HeaderBar);