import React from 'react';
import PageHeader from "../_base/PageHeader/PageHeader";

const Users = () => {
    return (
        <PageHeader title="Users" utilityComponent={null} />
    )
};

export default Users;