import React, {Fragment} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import FileCopyIcon from '@material-ui/icons/FileCopyOutlined';
import SaveIcon from '@material-ui/icons/Save';
import PrintIcon from '@material-ui/icons/Print';
import ShareIcon from '@material-ui/icons/Share';
import FavoriteIcon from '@material-ui/icons/Favorite';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Workflows from "../Workflows";
import HTML5Backend from 'react-dnd-html5-backend'
import {DndProvider} from "react-dnd";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import PageHeader from "../_base/PageHeader/PageHeader";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'block',
        padding: theme.spacing(4, 4),
        marginBottom: 12,
        color: '#777777',
    },
    hide: {
        display: 'none',
    },
    search: {
        color: '#777777'
    },
    tableWrapper: {
        maxHeight: 407,
        overflow: 'auto',
    },
    actions: {
        flex: 1,
        flexDirection: 'col',
        alignContent: 'flex-end',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        textAlign: 'right',
        zIndex: 1000,
        height: '90px',
        marginTop: '-20px',
        marginLeft: '150px'
    },
    appbar: {
        radius: 4
    },
    radioGroup: {
        margin: theme.spacing(1, 0),
    },
    speedDial: {
        position: 'relative',
        '&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
            bottom: theme.spacing(2),
            right: theme.spacing(2),
        },
        '&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight': {
            top: theme.spacing(2),
            left: theme.spacing(2),
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 300,
    },
    fullWidth: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 600,
    },
    section: {
        marginTop: '20px',
        marginBottom: '40px'
    },
    buttonCollection: {
        marginTop: '40px'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: '#33cc99',
        },
    },
    options: {
        fontWeight: 600
    },
    coreActions: {
        marginBottom: '14px'
    },
    workflows: {
    },
    workflowManagement: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        alignItems: 'flex-start'
    },
    workflowCard: {
        width: '90%',
        backgroundColor: '#b7d4ca',
        border: 'solid 1px #738672',
        padding: '12px',
        marginBottom: '40px'
    },
    workflowCardText: {
        fontSize: 'small',
        color: '#555555'
    },
    workflowh5: {
        marginTop: '0px',
        marginBottom: '8px'
    },
    indicator: {
        color: '#33cc99',
        backgroundColor: '#33cc99'
    }
}));

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            <Box p={3}>{children}</Box>
        </Typography>
    );
}

const actions = [
    { icon: <FileCopyIcon />, name: 'Copy' },
    { icon: <SaveIcon />, name: 'Save' },
    { icon: <PrintIcon />, name: 'Print' },
    { icon: <ShareIcon />, name: 'Share' },
    { icon: <FavoriteIcon />, name: 'Like' },
];

const AddPosting = () => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const [direction, setDirection] = React.useState('down');
    const [open, setOpen] = React.useState(false);
    const [hidden, setHidden] = React.useState(false);
    const [education, setEducation] = React.useState(false);
    const [resume, setResume] = React.useState(false);
    const [references, setReferences]  = React.useState(false);

    const handleDirectionChange = event => {
        setDirection(event.target.value);
    };

    const handleHiddenChange = event => {
        setHidden(event.target.checked);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const handleEducation = name => event => {
        setEducation({ ...education, [name]: event.target.checked });
    };

    const brands = [
        {
            "id": 100000,
            "name": "ALL BRANDS"
        },
        {
            "id": 100001,
            "name": "Apple American"
        },
        {
            "id": 100002,
            "name": "Bell American"
        },
        {
            "id": 100003,
            "name": "Pan American"
        },
        {
            "id": 100004,
            "name": "RB American"
        }
    ];

    const positions = [
        {
            "id": 10000,
            "name": "ALL POSITIONS"
        },
        {
            "id": 87013,
            "name": "A Salary Test INternal"
        },
        {
            "id": 19063,
            "name": "Assistant Store Manager test"
        },
        {
            "id": 17224,
            "name": "B.A. Engineer Test"
        },
        {
            "id": 87042,
            "name": "Big Co Internal Name !@#$%^*(){}';<>?"
        },
        {
            "id": 10092,
            "name": "Bobtail Driver QA (19+)"
        },
        {
            "id": 86520,
            "name": "burger buds position Template test"
        },
        {
            "id": 16448,
            "name": "CACHE JOBED"
        },
        {
            "id": 26218,
            "name": "Cashier - Internal"
        },
        {
            "id": 25884,
            "name": "Cashier for Buckle"
        },
        {
            "id": 87049,
            "name": "Crew Test"
        },
        {
            "id": 10454,
            "name": "Custodian"
        },
        {
            "id": 88168,
            "name": "Database Engineer"
        },
        {
            "id": 14644,
            "name": "Delivery Driver - Do Not Use"
        },
        {
            "id": 14650,
            "name": "Delivery Driver-Sample"
        },
        {
            "id": 41278,
            "name": "District Manager 1"
        },
        {
            "id": 10091,
            "name": "DOT Driver"
        },
        {
            "id": 15614,
            "name": "EEOC Test"
        },
        {
            "id": 44317,
            "name": "Employee Retention Coordinator"
        },
        {
            "id": 18741,
            "name": "Firebirds Data"
        },
        {
            "id": 60915,
            "name": "Forms UItest"
        },
        {
            "id": 39691,
            "name": "General Manager"
        },
        {
            "id": 17035,
            "name": "General Manager (1)"
        },
        {
            "id": 48726,
            "name": "HR Generalist"
        },
        {
            "id": 10093,
            "name": "Inbound Internet Sales"
        },
        {
            "id": 87012,
            "name": "Internal Salary Template"
        },
        {
            "id": 10087,
            "name": "Internet Customer Service"
        },
        {
            "id": 10088,
            "name": "Internet Processing"
        },
        {
            "id": 65210,
            "name": "IT support2"
        },
        {
            "id": 10090,
            "name": "Loss Prevention Auditor"
        },
        {
            "id": 50120,
            "name": "MSK - Cashier"
        },
        {
            "id": 10089,
            "name": "Order Puller"
        },
        {
            "id": 43185,
            "name": "Pharm Tech"
        },
        {
            "id": 86988,
            "name": "PLAT 2481 test"
        },
        {
            "id": 86998,
            "name": "PLAT 2481 test 2"
        },
        {
            "id": 16836,
            "name": "pos type"
        },
        {
            "id": 49582,
            "name": "R-a-C HR Generalist"
        },
        {
            "id": 12566,
            "name": "Regional Manager"
        },
        {
            "id": 54769,
            "name": "Retail Custodial Associate"
        },
        {
            "id": 12565,
            "name": "Salaried Template Test for Big Co"
        },
        {
            "id": 16838,
            "name": "Salaried1"
        },
        {
            "id": 15514,
            "name": "Salary Test 1"
        },
        {
            "id": 15517,
            "name": "Salary Test 2"
        },
        {
            "id": 15520,
            "name": "Salary test 3"
        },
        {
            "id": 10085,
            "name": "Sales"
        },
        {
            "id": 44316,
            "name": "Sanitation"
        },
        {
            "id": 10452,
            "name": "Shift Leader-HU"
        },
        {
            "id": 10086,
            "name": "Stock"
        },
        {
            "id": 11300,
            "name": "Store Cashier"
        },
        {
            "id": 15138,
            "name": "Team Leader"
        },
        {
            "id": 75420,
            "name": "Test"
        },
        {
            "id": 49163,
            "name": "Test (1)"
        },
        {
            "id": 86777,
            "name": "Test Position for Big Co"
        },
        {
            "id": 86809,
            "name": "Test Position for Big Co 2"
        },
        {
            "id": 49032,
            "name": "Test S"
        },
        {
            "id": 25511,
            "name": "Test Template"
        },
        {
            "id": 75422,
            "name": "Test Test"
        },
        {
            "id": 86706,
            "name": "TetsFafdsafdsa"
        },
        {
            "id": 48725,
            "name": "Warehouse II"
        },
        {
            "id": 10453,
            "name": "Warehouse Person"
        }
    ];

    // Top 100 films as rated by IMDb users. http://www.imdb.com/chart/top
    const top100Films = [
            {
                "id": 10000,
                "name": "ALL LOCATIONS"
            },
            {
                "id": 10422,
                "name": "000076-Matthews, NC"
            },
            {
                "id": 36132,
                "name": "029110 Roebuck"
            },
            {
                "id": 63594,
                "name": "1875 University"
            },
            {
                "id": 10417,
                "name": "31-Austin, TX"
            },
            {
                "id": 10418,
                "name": "37-Fort Worth-Central,TX"
            },
            {
                "id": 10416,
                "name": "40-Fort Worth-South,TX"
            },
            {
                "id": 41129,
                "name": "Apple Core SR Test"
            },
            {
                "id": 42289,
                "name": "Automation Test Location"
            },
            {
                "id": 187589,
                "name": "BigCoTest100"
            },
            {
                "id": 187590,
                "name": "BigCoTest101"
            },
            {
                "id": 187695,
                "name": "BigCoTest102"
            },
            {
                "id": 36133,
                "name": "Columbus 2450 Airport Thruway"
            },
            {
                "id": 10419,
                "name": "DC test location"
            },
            {
                "id": 36131,
                "name": "Decherd 1820 Dercherd Blvd."
            },
            {
                "id": 48522,
                "name": "IL Symmetry Demo"
            },
            {
                "id": 74605,
                "name": "Isaiah Test Location"
            },
            {
                "id": 48526,
                "name": "KY Symmetry Demo"
            },
            {
                "id": 36130,
                "name": "Leander 1545 Hwy 183"
            },
            {
                "id": 48525,
                "name": "MI Symmetry Demo"
            },
            {
                "id": 47584,
                "name": "New Test Location"
            },
            {
                "id": 10421,
                "name": "Newark test location, NJ"
            },
            {
                "id": 48521,
                "name": "NJ Symmetry Demo"
            },
            {
                "id": 48524,
                "name": "NY Symmetry Demo"
            },
            {
                "id": 48523,
                "name": "OH Symmetry Demo"
            },
            {
                "id": 48520,
                "name": "PA Symmetry Demo"
            },
            {
                "id": 10420,
                "name": "Pittsburgh Test Location"
            },
            {
                "id": 54252,
                "name": "Stater Bros. Test Location3"
            },
            {
                "id": 54253,
                "name": "Stater Bros. Test Location4"
            },
            {
                "id": 51225,
                "name": "Stater Bros. Test LocationB"
            },
            {
                "id": 77276,
                "name": "Te23dff-zzTen"
            },
            {
                "id": 184804,
                "name": "Test Prop's"
            },
            {
                "id": 188246,
                "name": "Test Property Name25"
            },
            {
                "id": 23763,
                "name": "Travel Centers of America"
            }
        ]
    ;

    return (
        <Fragment>
            <PageHeader title="AddPosting" utilityComponent={null} />
            {/*<Grid container>*/}
            {/*    <Grid item xs={9} className={classes.coreActions}>*/}
            {/*        <h2>AddPosting</h2>*/}
            {/*        <Button variant="contained" color="primary">Post Listings</Button>&nbsp;   <Button variant="contained" color="secondary">Post Templates</Button>*/}
            {/*    </Grid>*/}
            {/*    <Grid item xs={3} className={classes.actions}>*/}
            {/*        /!*<SpeedDial*!/*/}
            {/*        /!*    ariaLabel="SpeedDial example"*!/*/}
            {/*        /!*    className={classes.speedDial}*!/*/}
            {/*        /!*    hidden={hidden}*!/*/}
            {/*        /!*    icon={<SpeedDialIcon />}*!/*/}
            {/*        /!*    onClose={handleClose}*!/*/}
            {/*        /!*    onOpen={handleOpen}*!/*/}
            {/*        /!*    open={open}*!/*/}
            {/*        /!*    direction={direction}*!/*/}
            {/*        /!*>*!/*/}
            {/*        /!*    {actions.map(action => (*!/*/}
            {/*        /!*        <SpeedDialAction*!/*/}
            {/*        /!*            key={action.name}*!/*/}
            {/*        /!*            icon={action.icon}*!/*/}
            {/*        /!*            tooltipTitle={action.name}*!/*/}
            {/*        /!*            onClick={handleClose}*!/*/}
            {/*        /!*         title="New AddPosting"/>*!/*/}
            {/*        /!*    ))}*!/*/}
            {/*        /!*</SpeedDial>*!/*/}
            {/*    </Grid>*/}
            {/*</Grid>*/}
            <Paper className={classes.appbar}>
                <AppBar color="primary" position="static">
                    <Tabs value={value} onChange={handleChange} classes={{indicator: classes.indicator }} aria-label="simple tabs example">
                        <Tab label="1. Configuration Settings" />
                        <Tab label="2. Form Fields" />
                        <Tab label="3. Third-party Verification" />
                        <Tab label="4. Workflow Management" />
                        <Tab label="5. Publish" />
                    </Tabs>
                </AppBar>
                <TabPanel value={value} index={0}>
                    <h3>Configuration Settings</h3>
                    <p className={classes.section}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet massa eget odio consequat suscipit. Nulla ut porttitor quam, in gravida odio. Etiam molestie ut erat sit amet vestibulum. Praesent maximus pulvinar scelerisque. Vestibulum lobortis volutpat purus, non dapibus risus gravida eu. Donec non faucibus sem. Vivamus nec nisl non nisi lacinia pellentesque eu non justo. Ut non dui lacus.</p>
               <hr />
                    <h4>Template Name</h4>
                    <TextField
                        id="outlined-basic"
                        className={classes.textField}
                        label="Enter Template Name"
                        margin="normal"
                        variant="outlined"
                    />
                    <h4>Description</h4>
                    <TextField
                        id="outlined-basic"
                        className={classes.fullWidth}
                        label="Enter Description"
                        multiline
                        rowsMax="8"
                        fullWidth
                        margin="normal"
                        variant="outlined"
                    />
                    <h4>Brands</h4>
                    <div style={{ width: 500 }}>
                        <Autocomplete
                            multiple
                            id="tags-standard"
                            options={brands}
                            getOptionLabel={option => option.name}
                            defaultValue={[brands[0]]}
                            renderInput={params => (
                                <TextField
                                    {...params}
                                    variant="standard"
                                    label="Selected Locations"
                                    placeholder="Add Location"
                                    margin="normal"
                                    fullWidth
                                />
                            )}
                        />
                    </div>
                    <h4>Locations</h4>
                    <div style={{ width: 500 }}>
                        <Autocomplete
                            multiple
                            id="tags-standard"
                            options={top100Films}
                            getOptionLabel={option => option.name}
                            defaultValue={[top100Films[0]]}
                            renderInput={params => (
                                <TextField
                                    {...params}
                                    variant="standard"
                                    label="Selected Locations"
                                    placeholder="Add Location"
                                    margin="normal"
                                    fullWidth
                                />
                            )}
                        />
                    </div>
                    <h4>Positions</h4>
                    <div style={{ width: 500 }}>
                        <Autocomplete
                            multiple
                            id="tags-standard"
                            options={positions}
                            getOptionLabel={option => option.name}
                            defaultValue={[positions[0]]}
                            renderInput={params => (
                                <TextField
                                    {...params}
                                    variant="standard"
                                    label="Selected Positions"
                                    placeholder="Add Position"
                                    margin="normal"
                                    fullWidth
                                />
                            )}
                        />
                    </div>
                    <div className={classes.buttonCollection}>
                        <Button variant="contained" color="primary">SAVE</Button>&nbsp;<Button variant="contained" color="secondary">Publish</Button>
                    </div>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <h3>Form Fields</h3>
                    <p>Add default fields or your own questions. Standard questions are required by talentReef and cannot be edited or removed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet massa eget odio consequat suscipit. Nulla ut porttitor quam, in gravida odio. Etiam molestie ut erat sit amet vestibulum. Praesent maximus pulvinar scelerisque. Vestibulum lobortis volutpat purus, non dapibus risus gravida eu. </p>
                    <ul>
                        <li>First Name</li>
                        <li>Last Name</li>
                        <li>Email</li>
                        <li>Phone</li>
                    </ul>
                    <FormControlLabel
                        className={classes.options}
                        control={
                            <Checkbox
                                checked={education}
                                onChange={handleEducation(!education)}
                                value="education"
                                color="primary"
                            />
                        }
                        label="Resume"
                    />
                    <p className="options-desc">Collecting a resume either uploading it through the application or from a third party such as Indeed, LinkedIn, etc.</p>
                    <FormControlLabel
                        className={classes.options}
                        control={
                            <Checkbox
                                checked={education}
                                onChange={handleEducation(!education)}
                                value="education"
                                color="primary"
                            />
                        }
                        label="Education"
                    />
                    <p className="options-desc">High School, College or Trade School Information</p>
                    <FormControlLabel
                        className={classes.options}
                        control={
                            <Checkbox
                                checked={education}
                                onChange={handleEducation(!education)}
                                value="education"
                                color="primary"
                            />
                        }
                        label="References"
                    />
                    <p className="options-desc">Collect up to 3 references</p>
                    <div className={classes.buttonCollection}>
                        <Button variant="contained" color="primary">SAVE</Button>&nbsp;<Button variant="contained" color="secondary">Publish</Button>
                    </div>
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <h3>Third-party Verification</h3>
                </TabPanel>
                <TabPanel value={value} index={3}>
                    <h3>Workflow Management</h3>
                    <p className={classes.section}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet massa eget odio consequat suscipit. Nulla ut porttitor quam, in gravida odio. Etiam molestie ut erat sit amet vestibulum. Praesent maximus pulvinar scelerisque. Vestibulum lobortis volutpat purus, non dapibus risus gravida eu. Donec non faucibus sem. Vivamus nec nisl non nisi lacinia pellentesque eu non justo. Ut non dui lacus.</p>
                    <hr />
                    <Grid container className={classes.workflowManagement}>
                        <Grid item md={6} className={classes.workflows}>
                            <h4>Workflow Options</h4>
                            <DndProvider backend={HTML5Backend}>
                                <Workflows />
                            </DndProvider>
                        </Grid>
                        <Grid item md={6}>
                            <h4>Workflow Process</h4>
                            <Card className={classes.workflowCard}>
                                <CardContent>
                                    <h5 className={classes.workflowh5}>Application</h5>
                                    <p className={classes.workflowCardText}>Candidate fills out the job application.</p>
                                </CardContent>
                            </Card>
                            <Card className={classes.workflowCard}>
                                <CardContent>
                                    <h5 className={classes.workflowh5}>Background Check</h5>
                                    <p className={classes.workflowCardText}>E-Verify Background Check</p>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                    <div className={classes.buttonCollection}>
                        <Button variant="contained" color="primary">SAVE</Button>&nbsp;<Button variant="contained" color="secondary">Publish</Button>
                    </div>
                </TabPanel>
                <TabPanel value={value} index={4}>
                    <h3>Publish</h3>
                </TabPanel>
            </Paper>
        </Fragment>
    )
};

export default AddPosting;