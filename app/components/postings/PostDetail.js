import React from 'react';
import PageHeader from "../_base/PageHeader/PageHeader";

const PostDetail = () => {
    return (
        <PageHeader title="Post Detail" utilityComponent={null} />
    );
};

export default PostDetail;