import React, {Fragment} from 'react';
import ListContainer from "../_base/List/ListContainer";

const templateData = {
    "data": [
        {
            "id": 1,
            "status": true,
            "label": "Easy Apply",
            "associations": [
                "job 1", "job 2", "job 3", "job 4", "job 5", "job 6"
            ],
            "createdBy": "MVegas",
            "createdOn": "04/09/2014",
            "detailPath": "/postings/details/"
        },
        {
            "id": 2,
            "status": true,
            "label": "Hourly Workforce",
            "associations": [
                "job 1", "job 2", "job 3"
            ],
            "createdBy": "ABoyd",
            "createdOn": "09/20/2014",
            "detailPath": "/postings/details/"
        },
        {
            "id": 3,
            "status": true,
            "label": "Salaried Workforce",
            "associations": [
                "job 1", "job 2", "job 3", "job 4", "job 5", "job 6", "job 7", "job 8", "job 9", "job 10"
            ],
            "createdBy": "JFusco",
            "createdOn": "09/20/2014",
            "detailPath": "/postings/details/"
        }
    ]
};


const PostingsList = () => {
    return (
        <Fragment>
            <ListContainer listContent={templateData} />
        </Fragment>
    );
};

export default PostingsList;