import firebase from 'firebase';

export const config = {
    apiKey: "AIzaSyAEDhWM2McmEAa3-p1HtFRVWn_RtT8UsXA",
    authDomain: "tr-ux-demo.firebaseapp.com",
    databaseURL: "https://tr-ux-demo.firebaseio.com",
    projectId: "tr-ux-demo",
    storageBucket: "tr-ux-demo.appspot.com",
    messagingSenderId: "88942542104",
    appId: "1:88942542104:web:7a53fca86da2e58a5c326d"
};

const firebaseConfig = firebase.initializeApp(config);

export default firebaseConfig;