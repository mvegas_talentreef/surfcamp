import React from 'react';
import NewFileImport from "../components/NewFileImport";

const FileImportContainer = () => {
    return (
        <NewFileImport />
    )
};

export default FileImportContainer;