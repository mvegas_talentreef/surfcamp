import React from 'react';
import Integrations from "../components/Integrations";

const IntegrationsContainer = () => {
    return (
        <Integrations />
    )
};

export default IntegrationsContainer;