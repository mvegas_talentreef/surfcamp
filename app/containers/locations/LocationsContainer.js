import React from 'react';
import Locations from "../../components/locations/Locations";

const LocationsContainer = () => {
    return (
        <Locations />
    )
};

export default LocationsContainer;