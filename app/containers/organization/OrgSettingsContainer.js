import React from 'react';
import OrgSettings from '../../components/organization/OrgSettings';

const OrgSettingsContainer = () => {
    return (
        <OrgSettings />
    )
};


export default OrgSettingsContainer;