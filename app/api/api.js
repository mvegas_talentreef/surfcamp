// api.js
import Hapi from 'hapi';
import mongoose from 'mongoose';
import RestHapi from 'rest-hapi';

async function api(){
    try {
        let server = Hapi.Server({ port: 3001 });

        let config = {
            appTitle: "My API",
        };

        await server.register({
            plugin: RestHapi,
            options: {
                mongoose,
                config
            }
        });

        await server.start();

        console.log("Server ready", server.info);

        return server
    } catch (err) {
        console.log("Error starting server:", err);
    }
}

module.exports = api();