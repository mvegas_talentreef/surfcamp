export const getDateWithFormat = () => {
    const today = new Date();
    let DD = today.getDate();
    // January is 0!
    let MM = today.getMonth() + 1;
    let YYYY = today.getFullYear();

    if (DD < 10 ) {
        DD = '0' + DD;
    }

    if (MM < 10) {
        MM = '0' + MM;
    }

    return YYYY + MM + DD;
};

export const getCurrentTime = () => {
    const now = new Date();
    return now.getHours() + ":" + now.getMinutes();
};

export const mapOrder = (array, order, key) => {
    array.sort(function (a, b) {
        let A = a[key], B = b[key];
        if (order.indexOf(A + "") > order.indexOf(B + "")) {
            return 1;
        } else {
            return -1;
        }
    });
    return array;
};

