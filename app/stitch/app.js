import {RemoteMongoClient, Stitch} from 'mongodb-stitch-browser-sdk';

// the stitch App ID
const APP_ID = 'manage-cmukw';

// initialize the app client
const app = Stitch.hasAppClient(APP_ID)
    ? Stitch.getAppClient(APP_ID)
    : Stitch.initializeDefaultAppClient(APP_ID);

export { app };