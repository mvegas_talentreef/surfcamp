import { RemoteMongoClient, AnonymousCredential } from 'mongodb-stitch-browser-sdk';
import { app } from './app';

let listings = [];

app.auth.loginWithCredential(new AnonymousCredential()).then(() => {
    // initialize the mongoDB service client
});

export const mongoClient = app.getServiceClient(RemoteMongoClient.factory, 'mongodb-atlas').db('sample_airbnb').collection('listingsAndReviews');

// export const items = mongoClient.db('sample_airbnb').collection('listingsAndReviews').find({}, {limit: 50}).asArray();

// app.auth.loginWithCredential(new AnonymousCredential()).then(() =>
//         mongoClient.collection('listingsAndReviews').find({}, { limit: 50}).asArray()
//     //db.collection('listingsAndReviews').updateOne({owner_id: client.auth.user.id}, {$set:{number:42}}, {upsert:true})
// ).then(() =>
//     mongoClient.collection('listingsAndReviews').find({}, { limit: 50}).asArray()
// ).then(docs => {
//     console.log("Found docs", docs);
//     console.log("[MongoDB Stitch] Connected to Stitch");
//     listings = docs;
// }).catch(err => {
//     console.error(err)
// });
//
// export { mongoClient };