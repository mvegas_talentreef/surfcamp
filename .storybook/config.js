import {addDecorator, addParameters, configure} from '@storybook/react';
import { themes } from '@storybook/theming';
import trTheme from './trTheme';
import { addReadme } from 'storybook-readme';
import { withInfo } from '@storybook/addon-info';

addParameters({
    options: {
        theme: trTheme,
        panelPosition: 'bottom'
    },
});

addDecorator(withInfo);
addDecorator(addReadme);

// // automatically import all files ending in *.stories.js
// const req = require.context('../stories', true, /\.stories.js$/);
// function loadStories() {
//     req.keys().forEach(filename => req(filename));
// }

configure(require.context('../stories', true, /\.stories.js$/), module);
