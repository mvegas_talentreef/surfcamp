import { create } from "@storybook/theming";

export default create ({
    base: 'dark',
    mainTextFace: '"Ubuntu", sans-serif',
    colorPrimary: '#394360',
    colorSecondary: '#60677C',
    // UI
    appBg: '#394360',
    appContentBg: '#60677C',
    appBorderColor: 'white',
    appBorderRadius: 4,
    // Typography
    fontBase: '"Ubuntu", sans-serif',
    fontCode: 'monospace',
    // Text colors
    textColor: 'white',
    textInverseColor: 'white',
    textInput: '"Ubuntu", sans-serif',
    // Toolbar default and active colors
    barTextColor: 'white',
    barSelectedColor: 'white',
    barBg: '#33cc99',
    // Form colors
    inputBg: 'white',
    inputBorder: 'silver',
    inputTextColor: '#444444',
    inputBorderRadius: 4,
    brandTitle: 'talentReef Storybook',
    brandUrl: 'https://example.com',
    brandImage: 'https://tr-ux-demo.firebaseapp.com/images/talentReef_white.png'
});