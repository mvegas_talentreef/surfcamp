# SurfCamp: New React.js Project

## Getting Started

This is a hand-rolled React project, not a create-react-app project. 

#### Running locally

`npm install`

then run...

`npm start`

Navigate to:

`http://localhost:8080`

#### Tech Stack

- React ^16.9.0
- Hooks & Context API (State Management)
- JEST (Unit Testing)
- MongoDB
- Mongoose

#### Storybook

`npm run storybook`
