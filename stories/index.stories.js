import React from 'react';
import { storiesOf } from "@storybook/react";
import PageHeader from '../app/components/_base/PageHeader/PageHeader';
import PageHeaderReadme from '../app/components/_base/PageHeader/PageHeader.README.md';
import OneColumnLayout from "../app/components/_base/OneColumnLayout/OneColumnLayout";
import OneColumnLayoutReadme from '../app/components/_base/OneColumnLayout/OneColumnLayout.README.md';

import {withKnobs} from "@storybook/addon-knobs";

storiesOf('Page Blocks', module)
    .addDecorator(withKnobs)
    .addParameters({
        readme: {
            content: PageHeaderReadme,
            sidebar: PageHeaderReadme
        },
    })
    .add('Page Header', () => <PageHeader title="Dashboard" utilityComponent={null} />)
    .add('One Column Layout', () => <OneColumnLayout><h1>Demo Layout</h1></OneColumnLayout>);